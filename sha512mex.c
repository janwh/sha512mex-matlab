/*=================================================================
 *
 * sha512mex.C	Sample .MEX file corresponding to sha512mex.M
 *	        Solves simple 3 body orbit problem
 *
 * The calling syntax is:
 *
 *		[yp] = sha512mex(t, y)
 *
 *  You may also want to look at the corresponding M-code, sha512mex.m.
 *
 * This is a MEX-file for MATLAB.
 * Copyright 1984-2011 The MathWorks, Inc.
 *
 *=================================================================*/
#include <math.h>
#include "mex.h"
#include "sha512.h"
#include "sha512.c"

#define HASHLEN 64
#define HEXLEN HASHLEN*2

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray*prhs[])

{
    char *input_buf, *output_buf;
    unsigned char hash[HASHLEN];
    int error = 0;
    unsigned int i = 0, j = 0;;

    /* Check for proper number of arguments */
    if (nrhs != 1) {
        mexErrMsgIdAndTxt("MATLAB:sha512mex:invalidNumInputs",
                "Only 1  input argument allowed.");
    } else if (nlhs > 1) {
        mexErrMsgIdAndTxt("MATLAB:sha512mex:maxlhs",
                          "Too many output arguments.");
    }

    /* Check for proper type of input */
    if (!mxIsChar(prhs[0]))  {
        mexErrMsgIdAndTxt("MATLAB:sha512mex:invalidY",
                          "Filename needs to be a string.");
    }

    /* Assign i/o buffers */
    input_buf = mxArrayToString(prhs[0]);
    output_buf=mxCalloc(HEXLEN, sizeof(char));

    /* Do the actual hashing */
    error = sha512_file(input_buf, hash, 0);
    if (error != 0) {
        mexErrMsgIdAndTxt( "MATLAB:sha512mex:hashingFailed",
                "Hashing failed. I/O error.");
    }

    /* Turn byte string into hex */
    for (i = 0; i < HASHLEN; i++) {
        sprintf(output_buf+j, "%02x", hash[i]);
        j = j+2;
    }

    /* Assign output buffer to left hand arguments */
    plhs[0] = mxCreateString(output_buf);
    return;
}
