# sha512mex

MATLAB MEX wrapper for a C-implementation of SHA-512 hashing algorithm


## Outline

This function takes the path to any existing file and calculates the SHA-512 hashsum of it. It is basically a simple MEX wrapper for the SHA-512 implementation used in the [PolarSSL](https://github.com/polarssl/polarssl) library.

*Input*: `filename`: Name of the file to be hashsummed

*Output*: `hash`: The hashsum in hexadecimal format (128 chars long)

## Before use

**Please note that the `sha512mex` sources needs to be compiled using MEX in MATLAB. This requires a functioning build environment on your side. For up-to-date binaries of `sha512mex`, please checkout the releases page: <https://github.com/Janwillhaus/sha512mex/releases/latest>**

To compile `sha512mex` simply execute
```
mex sha512mex.c
```
at the MATLAB Command Prompt. Or execute the function itself. In a functioning build environment, the first execution does compile the function aswell.

## License

Released under GPL license. See LICENSE file for a complete copy.
