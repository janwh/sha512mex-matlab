function sha512mex(~)
%SHA512MEX Mex wrapper for a C-implementation of SHA-512 hashing algorithm
%   This function takes the path to any existing file and calculates the
%   SHA-512 hashsum of it. It is basically a simple MEX wrapper for the
%   SHA-512 implementation used in the PolarSSL library
%
%    Input : filename : Name of the file to be hashsummed
%   Output : hash     : The hashsum in hexadecimal format (128 chars long)
%
% Created 04.12.2014 by Jan Willhaus <mail@janwillhaus.de>
% Released under GPL license.

% If mex executable is not present: compile it
mex sha512mex.c